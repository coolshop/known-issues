# Known Issues

This page describes known issues with Coolshop and Cool PIM. Please use this
page before you submit a bug to IT.

## Coolshop
* **Critical** In some cases where you have multi level variants on a product,
where there is an overlap between variants in the 2nd (or further) level, it is
not possible to access these variants. *We plan on having a fix for this no
later than Monday afternoon*.
* **Minor** Sorting by newest products shows semi-wrong products. *We likely
will not be able to fix this, but it will "auto fix" itself over a week, due to
new products being created*.
* **Major** ~~Streetman products on both Coolshop and Streetman are missing buy
buttons on the search page, as well as stock information.~~
* **Major** Coolshop logos and text are shown in all Streetman e-mails being
sent from the shop and SAP.
* **Minor** All comments page does not work.
* **Critical** Coolshots do not work in any way.
* **Major** It is not possible to edit new prices or add new products to the
"used" application (Tradeshop). It's also unclear if SKUs or product IDs should
be used when sending out mass e-mails (e-mail Jacob when this is fixed/known).
* **Major** It is not possible to access the order list on the "my pages" in
some cases.
* **Critical** The norwegian site doesn't really work. *Fix is incoming, where
we will change the address from www.coolshop.com/no/ to no.coolshop.com*.
* **Critical** Search pages/campaigns break in Finland if you browse certain
pages.
* **Minor** Pre order keys seem very broken - cannot be accessed through Django
Admin.

### Coolshop Internal
* **Minor** It's not possible to see if an order was placed on Coolshop or on
Streetman.
* **Major** Orders and customers are not searchable. *We are actively working
on a fix, and will have it resolved as soon as possible*.

## Cool PIM
* **Minor** Under certain circumstances, if you add a product description it
might dissapear when you save the product. *Note that we have not been able to
identify the cause of this issues. If you experience this, please get hold of
IT*.
* **Major** Please be aware of a possible issue, where a section might be empty
. If this happens, get hold of IT immediately. Under no circumstance save the
page.
* **Major** ~~We are aware of an issue on the translations page, where
translating text of any kind can result in products being exported to SAP.
*For the time being, please do not use this feature. We are working on a fix,
and we expect it to be online no later than Tuesday*.~~
* **Major** There is an issue with updating attribute types and attributes
from the superuser interface. If this is done, it will trigger a lot of SAP
updates. Please do not use this feature if you can avoid it for now. *We
expect a fix no later than Tuesday*. If you urgently need something changed,
please contact IT.
* **Minor** ~~Sellable below zero stock and include in webshop does not show
correctly in the pricing view.~~
* **Minor** If you deleted or added product images before August 6, you might
experience that the changes will not be reflected in webshops. As a workaround,
just click "save changes" on the product in PIM, and it will be fixed. At some
point, we will run a full export on all products, to fix the issue permanently.
